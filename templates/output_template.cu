#include <math.h>
#include <iostream>
#include "cuda_runtime.h"
#include "kernel.h"
#include <stdlib.h>

using namespace std;

__global__ void matrixMultiplicationKernel(float* A, float* B, float* C, int A_rows, int A_cols, int B_cols) {
    int ROW = blockIdx.y * blockDim.y + threadIdx.y;
    int COL = blockIdx.x * blockDim.x + threadIdx.x;

    float tmpSum = 0;
    if (ROW < A_rows && COL < B_cols) {
        for (int i = 0; i < A_cols; i++) {
            tmpSum += A[ROW * A_cols + i] * B[i * B_cols + COL];
        }
        C[ROW * B_cols + COL] = tmpSum;
    }
}

void matrixMultiplication(float *A, float *B, float *C, int A_rows, int A_cols, int B_rows, int B_cols) {
    // Check for valid matrix dimensions
    if (A_cols != B_rows) {
        cerr << "Matrix dimensions do not allow multiplication (A_cols must be equal to B_rows)." << endl;
        return;
    }

    // Declare the number of blocks per grid and the number of threads per block
    dim3 threadsPerBlock(16, 16);
    dim3 blocksPerGrid((B_cols + threadsPerBlock.x - 1) / threadsPerBlock.x, 
                       (A_rows + threadsPerBlock.y - 1) / threadsPerBlock.y);

    matrixMultiplicationKernel<<<blocksPerGrid, threadsPerBlock>>>(A, B, C, A_rows, A_cols, B_cols);
}
