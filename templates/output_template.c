#include <stdio.h>
#include <stdlib.h>
// #include <omp.h>

void $filename$(int m, int n, int**C, int**A, int**B){
	// $omp$
    for(int i = 0; i < m; i++) {
        for(int k = 0; k < m; k++) {
            for(int j = 0; j < n; j++) {
                C[i][k] += A[i][j] * B[j][k];
            }
        }
    }
}
