#include <stdio.h>
#include "blis.h" // Need BLIS to run

int main( int argc, char** argv )
{
    dim_t m, n, k;
    inc_t rsa, csa;
    inc_t rsb, csb;
    inc_t rsc, csc;

    double* a;
    double* b;
    double* c;
    double zero = 0.0;
    double one = 1.0;
    double alpha = 1.0, beta = 1.0;

    m = $M$; n = $N$; k = $K$;
    rsc = 1; csc = m;
    rsa = 1; csa = m;
    rsb = 1; csb = k;

    a = malloc( m * k * sizeof( double ) );
    b = malloc( k * n * sizeof( double ) );
    c = malloc( m * n * sizeof( double ) );

    $FILL_A$
    $FILL_B$

    bli_dgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
               m, n, k, &alpha, a, rsa, csa, b, rsb, csb,
               &beta, c, rsc, csc );

    bli_dprintm( "c: after gemm", m, n, c, rsc, csc, "%4.1f", "" );

    free(a);
    free(b);
    free(c);

    return 0;
}
