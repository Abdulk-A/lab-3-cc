import json

#generate hand matrix based on json input row, col size
def generate_rolled_matrix(input_json):
    expressions = []
    rows = int(input_data[input_json]["matrix_1"]["rows"]) #eventually change to be dynamic
    cols = int(input_data[input_json]["matrix_2"]["rows"]) #eventually change to be dynamic
    for i in range(rows):
        for k in range(rows):
            s = f'C[{i}][{k}] = '
            for j in range(rows):
                s += f'(A[{i}][{j}] * B[{j}][{k}]) +'
            
            s = s[:-2]
            s += ';'
            expressions.append(s)
    return expressions

#creates the baseline .... Creates a .c file from output_template.c 
def create_file2(filename, omp):
    output = output_template.replace('$filename$', filename) 
    output = output.replace('$omp$', omp)

    with open(f'funcs/{filename}.c', 'w') as file:
        file.write(output)

#creates the output hand 
def create_file3(filename, omp):
    out_temp = ... 
    with open('templates/output_template_hand.c', 'r') as f:
        out_temp = f.read()

    output = out_temp.replace('$filename$', filename) 
    expr = generate_rolled_matrix(filename)
    expr_str = "\n".join(expr) 
    output = output.replace('$expression$', expr_str)
    output = output.replace('$omp$', omp)

    with open(f'funcs/{filename}.c', 'w') as file:
        file.write(output)

# Creates the cuda variant
def create_file4(filename):
    template_path = 'templates/output_template.cu'
    
    with open(template_path, 'r') as file:
        cuda_template = file.read()

    with open(f'funcs/{filename}.cu', 'w') as file:
        file.write(cuda_template)

# Creates the blis variant
def create_file5(filename, data):
    template_path = 'templates/blis_template.c'

    # Get the dimensions
    A_rows = data[filename]["matrix_1"]["rows"]
    A_cols = data[filename]["matrix_1"]["cols"]
    B_cols = data[filename]["matrix_2"]["cols"]
    A_data = data[filename]["matrix_1"]["data"]
    B_data = data[filename]["matrix_2"]["data"]
    
    fill_a = "\n    ".join(f"a[{i}] = {A_data[i]};" for i in range(A_rows * A_cols))
    fill_b = "\n    ".join(f"b[{i}] = {B_data[i]};" for i in range(A_cols * B_cols))

    with open(template_path, 'r') as file:
        code_template = file.read()

    code_filled = code_template.replace('$M$', str(A_rows))
    code_filled = code_filled.replace('$N$', str(B_cols))
    code_filled = code_filled.replace('$K$', str(A_cols)) 
    code_filled = code_filled.replace('$FILL_A$', fill_a)
    code_filled = code_filled.replace('$FILL_B$', fill_b)

    with open(f'funcs/{filename}.c', 'w') as file:
        file.write(code_filled)

#initialization, everything starts Here!

input_path = 'input.json'
output_template_path = 'templates/output_template.c'

# Read the json file 
input_data = ...
with open(input_path, 'r') as f:
    input_data = json.load(f)

# Read the template file 
#### changes to be made here
output_template = ... 
with open(output_template_path, 'r') as f: 
    output_template = f.read()

print(f'Input Program: {input_path}')
for file in input_data:
    print(f'Input Data: {input_data[file]}')
    print(f'Output Program: {file}.c')

    if file == "output_baseline":
        create_file2(file, '')
    if file == "output_hand":
        create_file3(file, '')
    if file == "output_cuda":
        create_file4(file)
    if file == "output_blis":
        create_file5(file, input_data)

    # Make the baseline funcation
    # expression = generate_matrix_multiplication(rules)
    # create_file(file, biggest_offset, expression, '')   #need a new create_file method


###########################################
# Generates funcs.h file for the tester
###########################################

# Read template file 

funcs_template = ...
with open('templates/funcs_template.h') as f:   
    funcs_template = f.read()

# Create the include and function lists

include_list = ''.join([f'#include \"{file}.c\"\n' for file in input_data])[:-1]
function_list = ''.join([f'\t{file},\n' for file in input_data])[:-1]

# Replace appropriate data 
funcs_output = funcs_template.replace('$include_list$', include_list)
funcs_output = funcs_output.replace('$function_list$', function_list)

# Write the funcs.h file
with open('./funcs/funcs.h', 'w') as file:
    file.write(funcs_output)  



#REFERENCE

# 
# def create_file(filename, out_size, expression, omp):
#     output = output_template.replace('$filename$', filename) 
#     # output = output.replace('$out_size$', str(out_size)) 
#     # output = output.replace('$expression$', expression)
#     output = output.replace('$omp$', omp)

#     with open(f'funcs/{filename}.c', 'w') as file:
#         file.write(output)


# Turns every number n in rules into a string: 'a[i + n]', then groups them with operations 
# def generate_math_string(rules):
#     string_rules = [[f'a[i + {offset}]' for offset in factor] for factor in rules] 
#     factors = ['(' + ' + '.join(factor) + ')' for factor in string_rules]
#     expression = ' * '.join(factors) 
#     return expression