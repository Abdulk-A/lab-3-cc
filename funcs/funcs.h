#include <stdio.h>
#include <stdlib.h>
#include "output_baseline.c"
#include "output_hand.c"
#include "output_cuda.c"
#include "output_blis.c"
void (*functionPointers[])(int, float*, float*) = {
	output_baseline,
	output_hand,
	output_cuda,
	output_blis,
	NULL
}; 
