#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "blis.h"

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Usage: %s [matrix_size]\n", argv[0]);
        return 1;
    }

    dim_t m, n, k;
    inc_t rsa, csa;
    inc_t rsb, csb;
    inc_t rsc, csc;

    double* a;
    double* b;
    double* c;
    double alpha = 1.0, beta = 0.0;
    double one = 1.0, zero = 0.0;

    m = n = k = atoi(argv[1]);
    rsa = csa = rsb = csb = rsc = csc = 1;

    a = (double*)malloc(m * k * sizeof(double));
    b = (double*)malloc(k * n * sizeof(double));
    c = (double*)malloc(m * n * sizeof(double));

    bli_drandm(0, BLIS_DENSE, m, k, a, rsa, csa);
    bli_drandm(0, BLIS_DENSE, k, n, b, rsb, csb);
    bli_dsetm(BLIS_NO_CONJUGATE, 0, BLIS_NONUNIT_DIAG, BLIS_DENSE,
              m, n, &zero, c, rsc, csc);

    struct timespec start, finish;
    double elapsed;

    clock_gettime(CLOCK_MONOTONIC, &start);
    
    bli_dgemm(BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
              m, n, k, &alpha, a, rsa, csa, b, rsb, csb,
              &beta, c, rsc, csc);

    clock_gettime(CLOCK_MONOTONIC, &finish);

    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

    printf("Average BLIS Time for size %d: %f seconds\n", m, elapsed);

    free(a);
    free(b);
    free(c);

    return 0;
}
