#include <iostream>
#include <cuda_runtime.h>
#include <cstdlib>
#include <ctime>

using namespace std;

__global__ void matrixMultiplicationKernel(float* A, float* B, float* C, int N) {
    int ROW = blockIdx.y * blockDim.y + threadIdx.y;
    int COL = blockIdx.x * blockDim.x + threadIdx.x;

    if (ROW < N && COL < N) {
        float tmpSum = 0;
        for (int i = 0; i < N; i++) {
            tmpSum += A[ROW * N + i] * B[i * N + COL];
        }
        C[ROW * N + COL] = tmpSum;
    }
}

void matrixMultiplication(float *A, float *B, float *C, int N) {
    // Declare the number of blocks per grid and the number of threads per block
    dim3 threadsPerBlock(16, 16);
    dim3 blocksPerGrid((N + threadsPerBlock.x - 1) / threadsPerBlock.x, 
                       (N + threadsPerBlock.y - 1) / threadsPerBlock.y);

    // Create CUDA events for timing
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float totalTime = 0.0;
    for (int i = 0; i < 1000; ++i) {
        cudaEventRecord(start);
        matrixMultiplicationKernel<<<blocksPerGrid, threadsPerBlock>>>(A, B, C, N);
        cudaEventRecord(stop);

        // Wait for kernel to finish
        cudaDeviceSynchronize();

        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        totalTime += milliseconds;
    }

    float averageTime = totalTime / 1000.0;
    cout << "Average CUDA Time for size " << N << ": " << averageTime << " ms\n";

    // Clean up
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

int main() {
    srand(time(NULL)); // Seed for random number generation

    int sizes[] = {5, 10, 15, 20, 25};
    for (int size : sizes) {
        float *A, *B, *C;

        // Allocate memory
        cudaMallocManaged(&A, size * size * sizeof(float));
        cudaMallocManaged(&B, size * size * sizeof(float));
        cudaMallocManaged(&C, size * size * sizeof(float));

        // Fill matrices with random values
        for (int i = 0; i < size * size; i++) {
            A[i] = rand() % 1000 / 100.0f;  // Random floats between 0 and 10
            B[i] = rand() % 1000 / 100.0f;
        }

        // Perform matrix multiplication
        matrixMultiplication(A, B, C, size);

        // Free memory
        cudaFree(A);
        cudaFree(B);
        cudaFree(C);
    }

    return 0;
}
