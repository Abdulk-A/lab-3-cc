import random, json, sys

def list_string(ls):
    return '{' + ', '.join([str(i) for i in ls]) + '}'

def matrix_multiplication(arr1, arr2, rows, cols):
    res = []
    for i in range(rows):
        for j in range(rows):
            result_ij = 0
            for k in range(cols):
                result_ij += arr1[i * cols + k] * arr2[k * rows + j]
            res.append(result_ij)
    return res

def create_line(index, rows, cols):
    vals1 = [random.randint(1, 10) for _ in range(rows * cols)]
    vals2 = [random.randint(1, 10) for _ in range(rows * cols)]
    outVals = matrix_multiplication(vals1, vals2, rows, cols)
    input1 = '(int[])' + list_string(vals1)
    input2 = '(int[])' + list_string(vals2)
    output = '(int[])' + list_string(outVals)
    return f'\t{list_string([index, rows, cols, input1, cols, rows, input2, rows, rows, output])},\\\n'

def add_line(index, row, col):
    lines = []
    for i in range(5):
        lines.append(create_line(index, row, col))
    return lines

def main():
    if len(sys.argv) != 2:
        print("Usage: python3 dataGen.py <dimension>")
        sys.exit(1)

    n = int(sys.argv[1])

    lines = add_line(0, n, n)  # Baseline index, will produce a n * n output matrix
    #lines += add_line(1, n, n)

    data_list = ''.join(lines)[:-1]

    # combine test data with template and save to TestData.c
    with open('templates/data_template.c', 'r') as template_file, \
         open('TestData.c', 'w') as test_data_file:
        template = template_file.read()
        test_data = template.replace('$data_list$', data_list)
        test_data_file.write(test_data)

if __name__ == "__main__":
    main()
