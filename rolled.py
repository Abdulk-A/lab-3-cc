
#generate hand matrix based on json input row, col size for square matrices
def generate_rolled_matrix(n):
    expressions = []

    for i in range(n):
        for k in range(n):
            s = f'C[{i}][{k}] = '
            for j in range(n):
                s += f'(A[{i}][{j}] * B[{j}][{k}]) +'
            
            s = s[:-2]
            s += ';'
            expressions.append(s)
    return expressions

