
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
// #include "funcs/funcs.h"
#include "funcs/output_baseline.c"
#include "funcs/output_hand.c"
#include "funcs/test_example.c"
#include "metaTest.c"
#include "TestData.c"

// Number of times a method is called with the same data
#define TEST_NUM 1000

struct TestInfo {
    int method;      

    int r1; 
    int c1;
    int * matrix_1; 
    
    int r2;
    int c2;
    int * matrix_2;
    
    int r3;
    int c3;     
    int * outputMatrix;    
} typedef TestInfo;