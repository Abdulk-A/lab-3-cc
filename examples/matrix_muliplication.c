
#include <stdlib.h>

//let m = 3 , n = 2
//1: 00 = (00 * 00) + (01 * 10)
//2: 01 = (00 * 01) + (01 * 11)
//3: 02 = (00 * 02) + (01 * 12)
void matrix_multiplication(int m, int n, int**C, int**A, int**B) {
    for(int i = 0; i < m; i++){
        for(int k = 0; k < m; k++) {
            for(int j = 0; j < n; j++) {
                C[i][k] += A[i][j] * B[j][k];
            }
        }
    }
}

//m and n not used
void matrix_multiplication_hand(int m, int n, int**C, int**A, int**B) {
    C[0][0] = (A[0][0] * B[0][0]) + (A[0][1] * B[1][0]);
    C[0][1] = (A[0][0] * B[0][1]) + (A[0][1] * B[1][1]);
    C[0][2] = (A[0][0] * B[0][2]) + (A[0][1] * B[1][2]);

    C[1][0] = (A[1][0] * B[0][0]) + (A[1][1] * B[1][0]);
    C[1][1] = (A[1][0] * B[0][1]) + (A[1][1] * B[1][1]);
    C[1][2] = (A[1][0] * B[0][2]) + (A[1][1] * B[1][2]);

    C[2][0] = (A[2][0] * B[0][0]) + (A[2][1] * B[1][0]);
    C[2][1] = (A[2][0] * B[0][1]) + (A[2][1] * B[1][1]);
    C[2][2] = (A[2][0] * B[0][2]) + (A[2][1] * B[1][2]);
}

void printMatrix(int**C, int m) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d ", C[i][j]);
        }
        printf("\n");
    }
}

int main() {

    int m = 3, n = 2; 

    //allocating memory
    int **A = (int **)malloc(m * sizeof(int *));
    int **B = (int **)malloc(n * sizeof(int *));
    int **C = (int **)malloc(m * sizeof(int *));
    for (int i = 0; i < m; i++) {
        A[i] = (int *)malloc(n * sizeof(int));
        C[i] = (int *)malloc(m * sizeof(int)); 
    }
    for (int i = 0; i < n; i++) {
        B[i] = (int *)malloc(m * sizeof(int)); 
    }

    A[0][0] = 1; A[0][1] = 2;
    A[1][0] = 3; A[1][1] = 4;
    A[2][0] = 5; A[2][1] = 6;

    B[0][0] = 7; B[0][1] = 8; B[0][2] = 9;
    B[1][0] = 10; B[1][1] = 11; B[1][2] = 12;

    matrix_multiplication(m, n, C, A, B);

    printMatrix(C, m);


    //free memory
    for (int i = 0; i < m; i++) {
        free(A[i]);
        free(C[i]);
    }
    for (int i = 0; i < n; i++) {
        free(B[i]);
    }
    free(A);
    free(B);
    free(C);

    return 0;
}

/*

A = 3 * 2    B = 2 * 3

Output = 3 * 3

A = 
[
a[0][0] a[0][1]
a[1][0] a[1][1]
a[2][0] a[2][1]
]

B = 
[
b[0][0] b[0][1] b[0][2] 
b[1][0] b[1][1] b[1][2] 
]

O=
[
o[0][0] o[0][1] o[0][2]
o[1][0] o[1][1] o[1][2]
o[2][0] o[2][1] o[2][2]
]

*/