#include <stdio.h>
#include "blis.h" // Need blis in order to run 
// Compile w/ gcc mm_blis.c -o mm -lblis -lm


int main( int argc, char** argv )
{
	dim_t m, n, k;
	inc_t rsa, csa;
	inc_t rsb, csb;
	inc_t rsc, csc;

	double* a;
	double* b;
	double* c;
	double  alpha, beta;

	// Initialize some basic constants.
	double zero = 0.0;
	double one  = 1.0;
	double two  = 2.0;


	//
	// This file demonstrates level-3 operations.
	//


	//
	// Example 1: Perform a general matrix-matrix multiply (gemm) operation.
	//

	printf( "\n#\n#  -- Example 1 --\n#\n\n" );

	// Create some matrix and vector operands to work with.
	m = 4; n = 5; k = 3;
	rsc = 1; csc = m;
	rsa = 1; csa = m;
	rsb = 1; csb = k;
	c = malloc( m * n * sizeof( double ) );
	a = malloc( m * k * sizeof( double ) );
	b = malloc( k * n * sizeof( double ) );

	// Set the scalars to use.
	alpha = 1.0;
	beta  = 1.0;

	// Initialize the matrix operands.
	bli_drandm( 0, BLIS_DENSE, m, k, a, rsa, csa );
	bli_dsetm( BLIS_NO_CONJUGATE, 0, BLIS_NONUNIT_DIAG, BLIS_DENSE,
               k, n, &one, b, rsb, csb );
	bli_dsetm( BLIS_NO_CONJUGATE, 0, BLIS_NONUNIT_DIAG, BLIS_DENSE,
               m, n, &zero, c, rsc, csc );

	bli_dprintm( "a: randomized", m, k, a, rsa, csa, "%4.1f", "" );
	bli_dprintm( "b: set to 1.0", k, n, b, rsb, csb, "%4.1f", "" );
	bli_dprintm( "c: initial value", m, n, c, rsc, csc, "%4.1f", "" );

	// c := beta * c + alpha * a * b, where 'a', 'b', and 'c' are general.
	bli_dgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
	           m, n, k, &alpha, a, rsa, csa, b, rsb, csb,
	                     &beta, c, rsc, csc );

	bli_dprintm( "c: after gemm", m, n, c, rsc, csc, "%4.1f", "" );

	// Free the memory obtained via malloc().
	free( a );
	free( b );
	free( c );

    return 0;
}
