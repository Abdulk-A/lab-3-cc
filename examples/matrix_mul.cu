#include <stdio.h>
#include <cuda_runtime.h>

// CUDA Kernel to multiply matrices
__global__ void matrixMultiply(float *A, float *B, float *C, int numARows, int numACols, int numBCols) {
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if (row < numARows && col < numBCols) {
        float Cvalue = 0;
        for (int e = 0; e < numACols; e++) {
            Cvalue += A[row * numACols + e] * B[e * numBCols + col];
        }
        C[row * numBCols + col] = Cvalue;
    }
}

int main() {
    // Define the dimensions of the matrices
    int numARows = 3, numACols = 2, numBRows = 2, numBCols = 3;

    // Allocate and initialize the matrices A, B, and C
    float A[3 * 2] = {1, 2, 3, 4, 5, 6};
    float B[2 * 3] = {7, 8, 9, 10, 11, 12};
    float C[3 * 3] = {0};

    float *d_A, *d_B, *d_C; // Device pointers

    size_t sizeA = numARows * numACols * sizeof(float);
    size_t sizeB = numBRows * numBCols * sizeof(float);
    size_t sizeC = numARows * numBCols * sizeof(float);

    // Allocate device memory
    cudaMalloc(&d_A, sizeA);
    cudaMalloc(&d_B, sizeB);
    cudaMalloc(&d_C, sizeC);

    // Copy host memory to device
    cudaMemcpy(d_A, A, sizeA, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, B, sizeB, cudaMemcpyHostToDevice);

    // Set up the execution configuration
    dim3 threadsPerBlock(3, 3);
    dim3 numBlocks((numBCols + threadsPerBlock.x - 1) / threadsPerBlock.x,
                   (numARows + threadsPerBlock.y - 1) / threadsPerBlock.y);

    // Launch the CUDA Kernel
    matrixMultiply<<<numBlocks, threadsPerBlock>>>(d_A, d_B, d_C, numARows, numACols, numBCols);

    // Copy result from device to host
    cudaMemcpy(C, d_C, sizeC, cudaMemcpyDeviceToHost);

    // Print the results
    for (int i = 0; i < numARows; i++) {
        for (int j = 0; j < numBCols; j++) {
            printf("%f ", C[i * numBCols + j]);
        }
        printf("\n");
    }

    // Free device memory
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    return 0;
}
