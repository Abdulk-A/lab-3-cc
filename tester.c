
#include "tester.h"

void init_matrix(int r, int c, int*vals, int***m, int isZro);
double mean_time(double*arr, int n);
void print_matrix(int**m, int r, int c);
double test_output(TestInfo t[], int n, int method);
bool verify_output(int**A, int**B, int r, int c);


void init_matrix(int r, int c, int*vals, int***m, int isZro) {

    *m = (int**)calloc(r, sizeof(int*));
    for(int i = 0; i < r; i++){
        (*m)[i] = (int*)calloc(c,sizeof(int));
    }
    int j = 0;
    if (isZro){

        for(int i = 0; i < r; i++){
            for(int k = 0; k < c; k++, j++){
                (*m)[i][k] = 0;
            }
        }

    }
    else {

        for(int i = 0; i < r; i++){
            for(int k = 0; k < c; k++, j++){
                (*m)[i][k] = vals[j];
            }
        }
    }

}

double mean_time(double*arr, int n){
    double sum = 0;
    for(int i = 0; i < n; i++) sum += arr[i];
    return sum / n;
}

void print_matrix(int**m, int r, int c) {
    printf("\n");
    for(int i = 0; i < r; i++){
        for(int k = 0; k < c; k++){
            printf("%d ", m[i][k]);
        }
        printf("\n");
    }
}

//n = method index, which is also stored in test info
double test_output(TestInfo t[], int n, int method) {

    clock_t start_t, end_t;
    double total_t;

    double aveTime[5];

    for(int i = n; i < n + 5; i++) { //5 for each method

        int ** A;
        init_matrix(t[i].r1, t[i].c1, t[i].matrix_1, &A, 0); //initilze matrix with matrix a values
        int ** B;
        init_matrix(t[i].r2, t[i].c2, t[i].matrix_2, &B, 0); //initilze matrix with matrix b values
        int ** C;
        init_matrix(t[i].r3, t[i].c3, t[i].outputMatrix, &C, 1); //initialize matrix of zeroes

        int ** output;
        init_matrix(t[i].r3, t[i].c3, t[i].outputMatrix, &output, 0);//initialize outputMatrix

        if(method == 0) output_baseline(t[i].r1, t[i].r2, C, A, B);
        else if(method == 1) output_hand(t[i].r1, t[i].r2, C, A, B);

        bool verified = verify_output(output, C, t[i].r3, t[i].c3);

        if(verified) printf("VERIFIED\n"); //verify matrix, will print 1 if they match else 0
        else printf("NOT VERIFIED\n"); 

        start_t = clock();

        for(int j = 0; j < TEST_NUM; j++) {          //run the same method for specific data 1000 times

            if(method == 0) output_baseline(t[i].r1, t[i].r2, C, A, B);
            else if(method == 1) output_hand(t[i].r1, t[i].r2, C, A, B);
        }

        end_t = clock();

        aveTime[i] = (double)(end_t - start_t) / CLOCKS_PER_SEC;
    }

    return mean_time(aveTime, 5);
}

bool verify_output(int**A, int**B, int r, int c) {

    for(int i = 0; i < r; i++){
        for(int k = 0; k < c; k++){
            if(A[i][k] != B[i][k]) return false;
        }
    }

    return true;
}

int main() {
    double aveTime[5] = {0};

    TestInfo t[] = TEST_DATA;

    //refer to testData.c for index for where method starts

    double baseline_time = test_output(t, 0, 0); //baseline
    printf("Average Baseline Time: %f\n", baseline_time);

    double unrolled_time = test_output(t,5,1); //unrolled 
    printf("Average unRolled Time: %f\n", unrolled_time);

    //double hand_blis = test_output(t, 1); // blis
    //printf("Average Hand Time: %f\n", blis_time);

    // double cuda_time = test_output(t, 5); //cuda
    // printf("Average CPU Time: %f\n", cuda_time);

    return 0;
}
