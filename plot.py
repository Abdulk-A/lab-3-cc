import subprocess
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import re

# Define matrix sizes to test
sizes = [5, 10, 15, 20, 25]

# Variants to test
variants = ["Baseline", "Blis"]

# Path to the CUDA and BLIS executables
cuda_executable = "./funcs/cuda"
blis_executable = "./funcs/blis"

# Results storage
results = {variant: [] for variant in variants}

def run_cuda_tests():
    # Run the CUDA executable and capture output
    result = subprocess.run([cuda_executable], capture_output=True, text=True)
    return result.stdout

def generate_test_data_and_compile(size):
    # Generate test data using dataGen.py
    subprocess.run(['python3', 'dataGen.py', str(size)], check=True)
    
    # Compile tester.c which includes the new test data
    compile_cmd = ['gcc', '-o', 'tester', 'tester.c']  # Add necessary libraries
    subprocess.run(compile_cmd, check=True)

def parse_time_from_output(output, size):
    times = {}
    for variant in variants:
        if variant == "Cuda":
            match = re.search(rf"Average CUDA Time for size {size}: ([\d.]+)", output)
            if match:
                times[variant] = float(match.group(1)) / 1000.0  # Convert ms to seconds
        elif variant == "Blis":
            match = re.search(rf"Average BLIS Time for size {size}: ([\d.]+)", output)
            if match:
                times[variant] = float(match.group(1))
        else:
            match = re.search(rf"Average {variant} Time: ([\d.]+)", output)
            if match:
                times[variant] = float(match.group(1))
    return times

def run_blis_tests(size):
    # Run the BLIS executable and capture output
    result = subprocess.run([blis_executable, str(size)], capture_output=True, text=True)
    return result.stdout

def test_variants(size):
    # Run the compiled tester program and capture output
    result = subprocess.run(['./tester'], capture_output=True, text=True)
    
    # Parse the output for times
    try:
        times = parse_time_from_output(result.stdout, size)
        return times
    except ValueError as e:
        print(f"Error parsing times: {e}")
        return None

# Retrieve CUDA results once as it is invariant across sizes
cuda_output = run_cuda_tests()

# Test each size
for size in sizes:
    # Generate and compile test data for Baseline (if applicable)
    generate_test_data_and_compile(size)
    # Run baseline and capture times
    times = test_variants(size)

    # Run BLIS tests and capture times
    blis_output = run_blis_tests(size)
    blis_times = parse_time_from_output(blis_output, size)

    # Collect times for all variants
    if times:
        for variant in variants:
            if variant == "Cuda":
                cuda_times = parse_time_from_output(cuda_output, size)
                results[variant].append(cuda_times[variant])
            elif variant == "Blis":
                results[variant].append(blis_times[variant])
            else:
                results[variant].append(times[variant])

# Plotting
for variant, times in results.items():
    plt.plot(sizes, times, label=variant)

plt.xlabel('Matrix Size')
plt.ylabel('Average Execution Time (seconds)')
plt.title('Matrix Multiplication Performance Comparison')

plt.gca().xaxis.set_major_formatter(ticker.FormatStrFormatter('%d'))

plt.legend()

# Save plot or show
plt.savefig('performance_comparison.png')
plt.show()
